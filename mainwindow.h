#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <qdebug.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_yaButton_clicked();

    void on_googleButton_clicked();

    void on_webView_statusBarMessage(const QString &text);

    void on_webView_loadProgress(int progress);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
