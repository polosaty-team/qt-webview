#include "mainwindow.h"
#include "ui_mainwindow.h"
//#include <QtWebKitWidgets/QWebView>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_yaButton_clicked()
{
    //ui->webView->setGeometry(0,0,200,200);
    ui->webView->load(QUrl("https://ya.ru/"));
    QNetworkAccessManager *mgr = ui->webView->page()->networkAccessManager();

    //ui->webView->load(QUrl("http://www.google.com"));
//     qDebug() <<
    //QWebPage *p = ui->webView->page();
    //p->

}

void MainWindow::on_googleButton_clicked()
{
    ui->webView->load(QUrl("http://www.google.com"));
}

void MainWindow::on_webView_statusBarMessage(const QString &text)
{
    qDebug() << text;
}

void MainWindow::on_webView_loadProgress(int progress)
{
    ui->progressBar->setValue(progress);
}
